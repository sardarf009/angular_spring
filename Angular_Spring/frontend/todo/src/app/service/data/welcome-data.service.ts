import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

export class HelloWorldBean {
  constructor(public msg:string){ }
}

@Injectable({
  providedIn: 'root'
})

export class WelcomeDataService {

  constructor(private http: HttpClient,

  ) { }

  executeHelloWorldBeanService() {

    return this.http.get<HelloWorldBean>("http://localhost:2020/hello-world");

  }



  executeHelloWorldBeanParamService(msg) {

     let basicAuthHeaderString = this.createBasicAuthenticationHttpHeader();
    let headers = new HttpHeaders({
         Authorization : basicAuthHeaderString
       })

       console.log(headers);
    return this.http.get<HelloWorldBean>(`http://localhost:2020/hello-world/${msg}`,{headers});

  }






 createBasicAuthenticationHttpHeader() {
   let username = 'admin'
   let password = 'admin'
   let basicAuthHeaderString = 'Basic '+window.btoa(username + ':' + password);

   return basicAuthHeaderString;
 }





}




