import {  HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Todo } from 'src/app/list-todos/list-todos.component';

@Injectable({
  providedIn: 'root'
})
export class TodoDataService {

  constructor(private http:HttpClient) { }



  findAllTodo() {
    return this.http.get<Todo[]>("http://localhost:2020/user/todo");

  }
  deleteById(id) {
    return this.http.delete(`http://localhost:2020/user/todo/${id}`);
  }

  getTodoById(id) {
    console.log(this.http.get<Todo>(`http://localhost:2020/user/todoData/${id}`));
    return this.http.get<Todo>(`http://localhost:2020/user/todoData/${id}`);
  }


  updateTodo(todo) {
    return this.http.put(`http://localhost:2020/user/todo/update`,todo);
  }



  createTodo(todo) {
    return this.http.post(`http://localhost:2020/user/todo/create`,todo);
  }
}



