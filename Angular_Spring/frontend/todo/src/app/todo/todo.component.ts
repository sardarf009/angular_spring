import { ActivatedRoute, Router } from '@angular/router';
import { getTestBed } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { TodoDataService } from '../service/data/todo-data.service';
import { Todo } from '../list-todos/list-todos.component';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
id:number;
todo:Todo;
  constructor(private dataService: TodoDataService,
    private route:ActivatedRoute,
    private router:Router
    ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.todo=new Todo(this.id,'',false,new Date());
    if(this.id!= -1)
   {
   console.log(this.route.snapshot.params['id']);
   this.dataService.getTodoById(this.id).subscribe(
    data=> this.todo = data
  )

}

  }

  saveTodo() {

    if(this.id == -1) { //=== ==
      console.log("create")
      this.dataService.createTodo(this.todo)
          .subscribe (
            data => {

              this.router.navigate(['todos'])
            }
          )
    } else {
      this.dataService.updateTodo(this.todo)
          .subscribe (
            data => {

              this.router.navigate(['todos'])
            }
          )
    }
  }


}




