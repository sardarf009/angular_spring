import { TodoDataService } from './../service/data/todo-data.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

//Future
// - No Navigation Menu and Footer
// - Formatting - Bootstrap
// - No Security for Menus
// - Hardcoded Logic in the TodoList and Login Components

// - Remaining Functionality - Edit, Delete, Add
// - Spring Boot
// - Spring Security

export class Todo {
  constructor(
    public id: number,
    public description: string,
    public done: boolean,
    public targetDate: Date
  ){

  }
}

@Component({
  selector: 'app-list-todos',
  templateUrl: './list-todos.component.html',
  styleUrls: ['./list-todos.component.css']
})
export class ListTodosComponent implements OnInit {

  todos=[];
  message;

  // todo = {
  //     id : 1,
  //     description: 'Learn to Dance'
  // }

  constructor(private service:TodoDataService,
     private route: Router) { }

  ngOnInit() {
    this.loadAndRefreshTodo();
  }

  loadAndRefreshTodo()
  {
    this.service.findAllTodo().subscribe(
      response => {
        this.todos= response;

      }      );
  }

  deleteTodo(id)
  {
    this.service.deleteById(id).subscribe(
   response => response
    );
    this.loadAndRefreshTodo();
    this.message= `Deleted Succsesfully ${id}`;

  }


  update(id)
  {
this.route.navigate(['todo',`${id}`]);
  }



  addTodo()
  {
    this.route.navigate(['todo',-1]);
  }

}
