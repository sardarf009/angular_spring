package com.lion.rest.lion.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.lion.rest.lion.service.TodoHardCodeService;
import com.lion.rest.model.lion.Todo;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class TodoResource {
	
	@Autowired
	private TodoHardCodeService todoHardCodeService;
	
	
	@GetMapping(value = "/user/todo")
	public List<Todo> getAllTodos()
	{
		
		return todoHardCodeService.fingAllTodo();
	}
	
	@DeleteMapping("/user/todo/{id}")
	public ResponseEntity<Void> deleteById(@PathVariable Long id) {
		Todo todo = todoHardCodeService.deleteById(id);
		if (todo != null) {
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.notFound().build();
	}
	
	
	@GetMapping("/user/todoData/{id}")
	public Todo getTodoById(@PathVariable Long id) {
		if(id!=null)
		return todoHardCodeService.getTodo(id);
		else
			return new Todo();
		
	}
	
	
	@PutMapping("/user/todo/update")
	public ResponseEntity<Todo> updateTodo(@RequestBody Todo todo) {
				if (todo != null) {
					
					Todo tododata=	todoHardCodeService.save(todo);
			
		}
		return new ResponseEntity<Todo>(todo,HttpStatus.OK);
	}
	
	@PostMapping("/user/todo/create")
	public ResponseEntity<Todo> createTodo(@RequestBody Todo todo) {
		Todo tododata=null;
		if (todo != null) {
			tododata = todoHardCodeService.save(todo);

		}
		URI uri=ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(tododata.getId()).toUri();
		return  ResponseEntity.created(uri).build();
	}
	

}
