package com.lion.rest.lion;

import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;



@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class Testing {

	@GetMapping(path = "/hello")
	public String helloWorld() {
		return "Hello World";
	}

	
	@GetMapping(path = "/hello-world")
	public Test2 testBean() {
		return new Test2("ddd");
	}
	
	
	@GetMapping(path = "/hello-world/{name}")
	public Test2 testBeanpathvariable(@PathVariable String name) {
		return new Test2(name);
	}
}
