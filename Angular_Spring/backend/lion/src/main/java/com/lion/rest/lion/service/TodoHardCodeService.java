package com.lion.rest.lion.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.lion.rest.model.lion.Todo;

@Service
public class TodoHardCodeService {
	static List<Todo> list=new ArrayList();
	
 static long id;
	
 
	static
	{
		list.add(new Todo(id++,"c++","descriptionc++",new Date(),false));
		list.add(new Todo(id++,"c","descriptionc",new Date(),false));
		list.add(new Todo(id++,"java","description java",new Date(),false));
		list.add(new Todo(id++,"python","description python",new Date(),false));
	}
	
	public List<Todo> fingAllTodo()
	{
		return list;
	}
	
	
	public Todo deleteById(Long id)
	{
		Todo todo = (id!=null) ? findById(id) : null;
		
		if(list.remove(todo))
		{
			return todo;
		}
			
		return null;
	}
	
	
	public Todo getTodo(Long id)
	{
		return findById(id);
	}
	
	
	public Todo save(Todo todo)
	{

		if(todo.getId()==-1)
		{			
			todo.setId(id++);
			list.add(todo);
		}else
		{
			deleteById(todo.getId());
			list.add(todo);
		}
		
		return todo;
		
	}
	
	
	private Todo findById(Long id2) {
	for(Todo todo:list)
	{
		if(todo.getId()==id2)
			return todo;
		
	}
		return null;
	}
	

}
